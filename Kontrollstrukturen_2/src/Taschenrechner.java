import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int Ergebnis;
		Scanner myScanner  = new Scanner (System.in);
		System.out.println("Geben Sie die erste Zahl ein");
		int a = myScanner.nextInt();
		System.out.println("Geben Sie die zweite Zahl ein");
		int b = myScanner.nextInt();
		
		System.out.println("Geben Sie eine dieser Rechenoperationen ein '+' '-' '*' '/'");
		char c = myScanner.next().charAt(0);
		
		switch (c)
		{
		case '+':
		Ergebnis = a + b;
		System.out.println(Ergebnis);
		break;
		
		case '-':
		Ergebnis = a - b;
		System.out.println(Ergebnis);
		break;
		
		case '*':
		Ergebnis = a * b;
		System.out.println(Ergebnis);
		break;
		
		case '/':
		Ergebnis = a / b;
		System.out.println(Ergebnis);
		break;
		
		default:
		System.out.println("Falsche Eingabe!");
		break;
		
		
		}

	}

}
