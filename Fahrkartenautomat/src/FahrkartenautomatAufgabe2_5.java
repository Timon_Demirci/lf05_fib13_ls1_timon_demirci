
import java.util.Scanner;

class FahrkartenautomatAufgabe2_5 {

	 public static void main(String[] args)									
	    {
	       Scanner tastatur = new Scanner(System.in);				
	      
	       double zuZahlenderBetrag; 											
	       double eingezahlterGesamtbetrag;										
	       double eingeworfeneM�nze;											
	       double r�ckgabebetrag;												
	       double preisFahrkarte;												
	       byte anzahlFahrkarten;											
	       
	       System.out.println("Guten Tag, willkommen bei der BVG! ");			
	       
	       System.out.print("Bitte geben Sie einen Preis f�r das Ticket ein, welches Sie kaufen m�chten (Bitte nur in 5 CENT [0,05] Schritten): "); 		// Nachfrage nach Preis des Tickets und springe in n�chste Zeile
	       preisFahrkarte = tastatur.nextDouble();
	       
	       System.out.println("");												// gebe leere Zeile aus
	       
	       System.out.print("Wie viele Fahrkarten m�chten Sie kaufen? (maximal 127 Tickets): ");		// gib Nachfrage zwecks Anzahl von Tickets aus
	       anzahlFahrkarten= tastatur.nextByte();														// einlesen von eingegebener Fahrkartenanzahl
	       
	       System.out.println(" ");												// gib eine leere Zeile aus
	       
	       zuZahlenderBetrag = preisFahrkarte * anzahlFahrkarten;				// Initialisierung der Variable zuZahlenderBetrag welche dem Ergebnis preisFahrkarte * anzahlFahrkarten entspricht 

	       System.out.printf("Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt: " + "%.2f EURO \n", zuZahlenderBetrag);		// Ausgabe von "Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt:" in EURO und mit 2 Kommastellen 
	       
	       System.out.println(" ");												// gib eine leere Zeile aus
	       
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = 0.0;										// Initialisierung der Variable eingezahlterGesamtbetrag mit Startwert 0.0
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)					// SOLANGE Variable eingezahlterGesamtbetrag < zuZahlenderBetrag
	       {
	    	   System.out.printf("Es sind noch " + "%.2f EURO zu zahlen! \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));		// gib aus "Es sind noch ... EURO zu zahlen: " mit verrechnetem Restwert
	    	   System.out.println(" ");
	    	   System.out.print("Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): ");						// gib aus "Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): "
	    	   eingeworfeneM�nze = tastatur.nextDouble();																			// Initialisierung der Variable eingeworfene M�nze als Eingabe des Users als Kommazahl
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;																		// addiere zu Variable eingezahlterGesamtbetrag den Wert von der Variable eingeworfene M�nze dazu
	       }

	       // Fahrscheinausgabe																					// NEUE FUNKTION
	       // -----------------
	       System.out.println("\nFahrschein wird gedruckt:");													// gib aus Fahrschein wird ausgegeben
	       for (int i = 0; i < 8; i++)																			// Variable i wird als integer deklariert mit dem Wert 0 (int i = 0), solange i < 8 ist, Wert 1 hinzuz�hlen (i++)
	       {
	          System.out.print("=");																			// gib "=" aus
	          try {
				Thread.sleep(250);																				// Verz�gerung beim ausgeben damit ein Ladeeffekt erzeugt wird
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");																			// 2 leere Zeilen werden untereinander ausgegeben

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;										// Variable r�ckgabebetrag wird initialisiert und soll mit der Verrechnung von eingezahlterGesamtbetrag - zuZahlenderBetrag gef�llt werden
	       if(r�ckgabebetrag > 0.0)																				// falls r�ckgabebetrag > 0.0 dann... 
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von: \n" + "%.2f EURO", r�ckgabebetrag);			// gib aus "Der R�ckgabebetrag in H�he von: " (mit einem horizentalen tab) + r�ckgabebetrag mit 2 Kommastellen + " EURO"
	    	   System.out.println("\n");																		// gib aus Leerzeile
	    	   System.out.println("Dieser wird in folgenden M�nzen ausgezahlt: \n");									// gib aus "Wird in folgenden M�nzen ausgezahlt: " (mit einem horizentalen tab))
	    	       	   
	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen													// wenn r�ckgabebetrag >= 2.0 dann
	           {
	        	  System.out.println("2,00 EURO");																// gib aus "2 EURO"
		          r�ckgabebetrag -= 2.0;																		// ziehe den Wert 2.0 von der Variable r�ckgabebetrag ab
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen													// wenn r�ckgabebetrag >= 1.0 dann
	           {
	        	  System.out.println("1,00 EURO");																// gib aus "1 EURO"
		          r�ckgabebetrag -= 1.0;																		// ziehe den Wert 1.0 von der Variable r�ckgabebetrag ab
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.5 dann
	           {
	        	  System.out.println("0,50 CENT");																// gib aus "50 CENT"
		          r�ckgabebetrag -= 0.5;																		// ziehe den Wert 0.5 von der Variable r�ckgabebetrag ab
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.2 dann
	           {
	        	  System.out.println("0,20 CENT");																// gib aus "20 CENT"
	 	          r�ckgabebetrag -= 0.2;																		// ziehe den Wert 0.2 von der Variable r�ckgabebetrag ab
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.1 dann
	           {
	        	  System.out.println("0,10 CENT");																// gib aus "10 CENT"
		          r�ckgabebetrag -= 0.1;																		// ziehe den Wert 0.1 von der Variable r�ckgabebetrag ab
	           }
	           while(r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen													// wenn r�ckgabebetrag >= 0.05 dann
	           {
	        	  System.out.println("0,05 CENT");																// gib aus "5 CENT"
	 	          r�ckgabebetrag -= 0.05;																		// ziehe den Wert 0.05 von der Variable r�ckgabebetrag ab
	           }
	       }
	    }
}
