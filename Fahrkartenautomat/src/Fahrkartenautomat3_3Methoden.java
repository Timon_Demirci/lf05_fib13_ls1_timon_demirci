
import java.util.Scanner;

class Fahrkartenautomat3_3Methoden {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double r�ckgabebetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen(); 
		r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		r�ckgeldAusgeben(r�ckgabebetrag);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n" + "Wir w�nschen Ihnen eine gute Fahrt.");

	}
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Guten Tag, willkommen bei der BVG! ");
		System.out.print("Bitte geben Sie einen Preis f�r das Ticket ein, welches Sie kaufen m�chten (Bitte nur in 5 CENT [0,05] Schritten): "); 
		double preisFahrkarte = tastatur.nextDouble();
		System.out.println("");
		System.out.print("Wie viele Fahrkarten m�chten Sie kaufen? (maximal 127 Tickets): ");
		int anzahlFahrkarten = tastatur.nextByte();
		System.out.println(" ");
		double zuZahlenderBetrag = preisFahrkarte * anzahlFahrkarten;
		System.out.printf("Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt: " + "%.2f EURO \n", zuZahlenderBetrag);
		System.out.println(" ");
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{System.out.printf("Es sind noch " + "%.2f EURO zu zahlen! \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));																														
		System.out.println(" ");
		System.out.print("Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): ");
		double eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return r�ckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird gedruckt:"); // gib aus Fahrschein wird ausgegeben
		for (int i = 0; i < 8; i++) // Variable i wird als integer deklariert mit dem Wert 0 (int i = 0), solange i
									// < 8 ist, Wert 1 hinzuz�hlen (i++)
		{
			System.out.print("="); // gib "=" aus
			try {
				Thread.sleep(250); // Verz�gerung beim ausgeben damit ein Ladeeffekt erzeugt wird
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void r�ckgeldAusgeben(double r�ckgabebetrag) {
		
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von: \n" + "%.2f EURO", r�ckgabebetrag);
			System.out.println("\n");
			System.out.println("Dieser wird in folgenden M�nzen ausgezahlt: \n");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2,00 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1,00 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("0,50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("0,20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("0,10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen
			{
				System.out.println("0,05 CENT");
				r�ckgabebetrag -= 0.05;

			}
		}
	}
}